﻿SystemUtil.CloseProcessByName "msedge.exe" @@ script infofile_;_ZIP::ssf2.xml_;_
SystemUtil.Run "C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe", "https://www.labanquepostale.fr/"
Browser("Banque et Assurance -").Page("Banque et Assurance -").Link("Mettre de l'argent de").Click @@ script infofile_;_ZIP::ssf16.xml_;_
Browser("Banque et Assurance -").Page("Compte Épargne - Placements").Link("Découvrir tous nos livrets").Click @@ script infofile_;_ZIP::ssf17.xml_;_
Browser("Banque et Assurance -").Page("Livrets d'épargne - La").Link("En savoir plus sur le").Click @@ script infofile_;_ZIP::ssf18.xml_;_
Browser("Banque et Assurance -").Page("Compte sur livret - La").WebElement("Comment ouvrir un compte").Check CheckPoint("Comment ouvrir un compte sur livret ?") @@ script infofile_;_ZIP::ssf19.xml_;_
Browser("Banque et Assurance -").Page("Compte sur livret - La").WebElement("Déjà client de La Banque").Check CheckPoint("Déjà client de La Banque Postale ?") @@ script infofile_;_ZIP::ssf23.xml_;_
DataTable.Value ("PathFile", dtGlobalSheet) = Environment("TestDir") & "\KBIS_Berthier_Fille.pdf"
FileContent("KBIS_Berthier_Fille.pdf").Check CheckPoint("KBIS_Berthier_Fille.pdf")
